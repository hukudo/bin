# Welcome to `~/hukudo/bin`
This is a collection of [Bash](https://www.gnu.org/software/bash/) and
[Python](https://www.python.org/) scripts that we find useful in our day-to-day
business.


## Installation
To install these scripts manually, simply put them on your `PATH`.

If you prefer to copy & paste our opinionated way to install these:
```
# Namespaces are one honking great idea -- let's do more of those!
# -- The Zen of Python
mkdir -p ~/hukudo

# clone
cd ~/hukudo
git clone https://gitlab.com/hukudo/bin.git

# add to PATH
cat <<'EOF' >> ~/.bashrc
[[ -d ~/hukudo/bin ]] && export PATH=$PATH:~/hukudo/bin
EOF
```


## Update
```
cd ~/hukudo/bin
git pull --ff-only
```


## Hackability
We want to keep scripts as **hackable** as possible. This means:

- We **keep parameterization to a minimum**; environment variables are OK, but
  we do not use things like [getopts](https://en.wikipedia.org/wiki/Getopts).
- We **do not provide help and usage docs**. The source code itself is the usage
  and help documentation, so instead of running `hukudo-foo --help`, you would
  run `$EDITOR $(which hukudo-foo)`.
- To **model dependencies** we use Bash's built-in `hash` function. Suppose you
  do not have `jq` installed and want to run `hukudo-gitlab-docker-tags`, you
  would get the output `bash: hash: jq: not found`, and you would know [what to
  do][install_jq].
- We **do not log info or debug output**, instead we run a script using Bash's
  `-x` flag to see what's happening, e.g.
  ```
  bash -x $(which hukudo-kubectl-context-create-for-namespace) foo
  ```
- We do of course use **readable variable names** and **comments with examples**
  to make the source **as readable as possible**.

[install_jq]: https://duckduckgo.com/?q=install+jq

Note that it is worthwhile to read Bash's manual in full (`man 1 bash`). It is
very well written and you get a good grasp of Bash's power.
